package main.scala.com.myntra.analytics

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Helper {

  def getSparkStreamingContext(): StreamingContext = {
    val conf = new SparkConf()
    conf.setAppName("kafka_reader-1.0").setMaster("local[*]").setAppName("HottestBrand")
    val sparkContext = new SparkContext(conf)
    sparkContext.setLogLevel("ERROR")
    new StreamingContext(sparkContext, Seconds(5))
  }

  def getKafkaStream(streamingContext: StreamingContext, topic: String, bootstrapServer: String): InputDStream[ConsumerRecord[String, String]] = {
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> bootstrapServer,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "event_consumer_group",
      "auto.offset.reset" -> "latest"
    )

    val topicSet = Set(topic)

    KafkaUtils.createDirectStream[String, String](
      streamingContext,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicSet, kafkaParams)
    )
  }
}
