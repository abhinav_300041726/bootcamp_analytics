package main.scala.com.myntra.analytics

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.InputDStream

import scala.util.Try

object HottestBrand {

  def main(args: Array[String]): Unit = {

    val streamingContext: StreamingContext = Helper.getSparkStreamingContext()

    val stream: InputDStream[ConsumerRecord[String, String]] = Helper.getKafkaStream(streamingContext, "test", "localhost:9092")

    stream.foreachRDD {
      rdd =>
        val top = Try {
          rdd.map {
            keyVal =>
              val eventFields = keyVal.value().split(",")
              val brand = eventFields(1)
              (brand, 1)
          }.reduceByKey(_ + _)
            .sortBy(_._2, ascending = false)
            .first()
        } getOrElse {
          ("NO_DATA", 0)
        }
        println(top._1 + "--->" + top._2)
    }
    streamingContext.start()
    streamingContext.awaitTermination()
  }
}
