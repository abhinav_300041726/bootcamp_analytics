package main.scala.com.myntra.analytics.application.controller;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Properties;

@Controller
@RequestMapping(value = "/demo")
public class DemoController {

    @GetMapping
    public ResponseEntity<Object> pushToKafka(
            @RequestParam(value = "app") String app,
            @RequestParam(value = "brand") String brand,
            @RequestParam(value = "event-type") String event_type,
            @RequestParam(value = "style-id") String style_id) throws Exception {
        String topic = "test";
        KafkaProducer producer = new KafkaProducer<String, String>(getProperties());

        producer.send(new ProducerRecord(topic, "key",
                                         app + "," + brand + "," + event_type + "," + style_id));
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    private Properties getProperties() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("acks", "1");
        return props;
    }
}
