package main.scala.com.myntra.analytics

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.util.Random

object EventGenerator {

  private def getProperties = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("acks", "1")
    props
  }

  def main(args: Array[String]): Unit = {
    val topic = "test"
    val eventTypes = List("addToCart", "listBoxClick")
    val brands = List("Roadster", "Highlander", "WROGN", "Ecko", "HERE&NOW")
    val apps = List("Android", "iOS", "Web")
    val random = new Random()
    val producer = new KafkaProducer[String, String](getProperties)
    while(true) {
      producer.send(new ProducerRecord(topic, "key",
        apps(random.nextInt(3)) + "," +
          brands(random.nextInt(5)) + "," +
          eventTypes(random.nextInt(2)) + "," +
          "1000000"))
      Thread.sleep(100)
    }

  }
}
