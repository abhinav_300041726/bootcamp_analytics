# Bootcamp Analytics

Pre-requisites:
Must have intellij, java 8, git installed.

Steps:
1. Clone the repo: https://bitbucket.org/abhinav_300041726/bootcamp_analytics/src/master/
2. Goto File -> Project Structure -> Platform Settings -> SDK -> Add 1.8s, remove all others.
3. Goto File -> Project Structure -> Libraries -> + -> Scala SDK -> Add 2.11.x, remove all others.
4. Add pom.xml to your maven projects. 
5. Do a mvn clean and then mvn install. Wait for imports.
6. Mark src/main/scala directory as sources root.

Create a new feature branch on your local system, and implement a spark streaming application which would print the 'hottest brand' in the last 5 seconds.
Steps:
1. File KafkaZKUtils is for starting up a local kafka in your machine. Run this first.
2. File EventGenerator is for creating random events in the kafka. Run this next.
3. You can also use the /demo API to send elements to kafka. (For this, you will have to set up spring boot in the project and run DemoApplication) 
4. File HottestBrand is for processing the data. Run this finally.